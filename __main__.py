import uvicorn

uvicorn.run("main:app", reload=True, access_log=False)