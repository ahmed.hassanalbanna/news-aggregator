class NewsAggregator():
  '''
    the news aggregator class unifies all the responses from configured apis into one.
    
    params:
      aggregators:list A list of aggregator instances to query from.
    methods:
      unify:
        params: result:list(list())
        returns: list
      query:
        params: q:str
        returns: list
      trending:
        returns: list
  '''
  def __init__(self,aggregators):
    self.aggregators = aggregators
    
  def unify(self,result):
    '''
      unifies a list of lists into on list with elements symetrically sorted to show news from
      different sources at the same pace.
      params: result:list(list()) #The list of lists of news
      returns: list #The unified list
    '''
    unified = []
    maxResponse = max([len(response) for response in result])
    for i in range(0,maxResponse):
      for response in result:
        if(len(response) > i):
          unified.append(response[i])
    return unified
  
  def query(self,q):
    '''
      Queries all the apis with the provided string query and returns the result
      params:
        q:str the query string to be passed to the apis
      returns: list #result from the apis
    '''
    result = []
    for aggregator in self.aggregators:
      response = aggregator.query(q)
      if response:
        result.append(response)
    return self.unify(result)
  
  def trending(self):
    '''
      Queries all the configured apis for trending news and returns the result
      returns: list #result from the apis
    '''
    result = []
    for aggregator in self.aggregators:
      response = aggregator.trending()
      if response:
        result.append(response)
    return self.unify(result)