from typing import Optional
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from newsapiAggregator import NewsapiAggregator
from newsAggregator import NewsAggregator
from redditAggregator import RedditAggregator
import settings

#list containing news aggregators that are enabled.
aggregators = [
  NewsapiAggregator(settings.newsapiConfig),
  RedditAggregator(settings.redditConfig)
]

#api unifier instance
news=NewsAggregator(aggregators)

app = FastAPI()

@app.get("/news")
def get_news(query: Optional[str]=None):
  '''
    sends the unified json response upon querying the apis via the /news handle.
    If no query was passed it returns the unified trending news as json.
  '''
  if query:
    return news.query(query)
  return news.trending()