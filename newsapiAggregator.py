import requests
from urllib import parse
import json
from abstractNewsSource import AbstractNewsSource
class NewsapiAggregator(AbstractNewsSource):
  '''
    This class provides methods to query newsapi.
    params:
      config:str The configuration object. This should be passed from the settings.py file upon instantiation.
        Example config:
        config = {
          "apiKey": "<Your_Key>", #your newsapi key
          "searchUrl": "https://newsapi.org/v2/everything", #the url used for news search (query method)
          "trendingUrl":"https://newsapi.org/v2/top-headlines", #the url used for getting treding news (trending method)
          "language": "en", needed for use by the (trending) method to display locally trending news
          "enabled": True,
            You shouldn't add this field to config as it is automatically set to true upon instatiation.
            You can however use it to control the api behavior.
            If set to False this api will not return any results.
        }
    methods:
      sendRequest:
        params: url:str params:str
        returns: str | None
      query:
        params: q:str
        returns: response:str | None
      trending:
        returns: str | None
      parseResponse:
        params: response:str
        returns: list | None
  '''
  def __init__(self, config):
    super().__init__(config)
    requiredFields = ("apiKey","language")
    if not all(field in config for field in requiredFields):
      print("There is an error in the newsapi configuration. Please fix it and try again.")
      print("required fields: "+requiredFields)
      self.enabled = False
    else:
      self.apiKey = config["apiKey"]
      self.language = config["language"]
      self.enabled = True
  
  def parseResponse(self, response):
    '''
      parses a response string from newsapi into a list containing:
      headline, link and source.
      [
        {
          "headline": "Skirting US sanctions, Cubans flock to cryptocurrency to shop online, send funds",
          "link": "https://www.channelnewsasia.com/news/business/skirting-us-sanctions--cubans-flock-to-cryptocurrency-to-shop-online--send-funds-11901148",
          "source": "newsapi"
        },
        ...
      ]
      params:
        response:str the text response from a newsapi query.
      returns:
        list: list of parsed dicts from the response text.
    '''
    response = json.loads(response)["articles"]
    result = []
    for post in response:
      result.append({
        "headline": post["title"],
        "link": post["url"],
        "source": "newsapi",
        #in case you want to add more data
        #"thumbnail": post["urlToImage"],
        #"content": post["content"]
      })
    return result
  
  def sendRequest(self,url,params={}):
    '''
      Sends a request to newsapi with the configured api key and the passed parameters.
      params:
        url:str the url to send the request to.
        params:dict a dictionary of newsapi parameters.
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    if self.enabled:
      params.update({"apiKey": self.apiKey,"pageSize":25})
      response = requests.get(url,params=params).text
      response = self.parseResponse(response)
      return response if response else None
    else:
      return None
    
  def query(self,q):
    '''
      query the newsapi.
      params:
        q:str the query string to be passed to the api
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    return self.sendRequest(self.searchUrl,{"q":parse.quote_plus(q)})
  def trending(self):
    '''
      get the trending news from newsapi.
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    return self.sendRequest(self.trendingUrl,{"language":self.language})

'''
#test  
import settings
reddit=NewsapiAggregator(settings.newsapiConfig)
print(len(reddit.trending())) #should be 25
print(len(reddit.query("hi"))) #should be 25
'''