from abc import abstractmethod
class AbstractNewsSource():
  def __init__(self,config) -> None:
    '''
      Abstract news source. override query() and trending() to suit your needs.
    '''
    requiredFields = ("searchUrl","trendingUrl")
    if not all(field in config for field in requiredFields):
      print("There is an error in the newsapi configuration. Please fix it and try again.")
      print("required fields: "+requiredFields)
      self.enabled = False
    else:
      self.searchUrl = config['searchUrl']
      self.trendingUrl = config['trendingUrl']
  @abstractmethod
  def query(self,q):
    pass
  @abstractmethod
  def trending(self):
    pass