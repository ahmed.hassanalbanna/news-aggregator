import requests
from urllib import parse
import json
import time
from abstractNewsSource import AbstractNewsSource
class RedditAggregator(AbstractNewsSource):
  '''
    This class provides methods to query reddit.
    params:
      config:str The configuration object. This should be passed from the settings.py file upon instantiation.
        Example config:
        config = {
          "username": "your-username",
          "password": "yourpassword",
          "id": "your client id from the reddit app",
          "secret": "you reddit app secret",
          "authUrl": "https://ssl.reddit.com/api/v1/access_token",
          "trendingUrl": "https://oauth.reddit.com/r/news",
          "searchUrl": "https://oauth.reddit.com/r/news/search"
        }
    methods:
      sendRequest:
        params: url:str params:str
        returns: str | None
      query:
        params: q:str
        returns: response:str | None
      trending:
        returns: str | None
      parseResponse:
        params: response:str
        returns: list | None
  '''
  def __init__(self, config):
    super().__init__(config)
    requiredFields = ("username","password","id","secret")
    if not all(field in config for field in requiredFields):
      print("There is an error in the reddit configuration. Please fix it and try again.")
      print("required fields: "+requiredFields)
      self.enabled = False
    else:
      self.username = config["username"]
      self.password = config["password"]
      self.id = config["id"]
      self.secret = config["secret"]
      self.authUrl = config["authUrl"]
      #self.searchUrl = config["searchUrl"]
      #self.trendingUrl = config["trendingUrl"]
      self.authorizedAt = 0
      self.authExpires = 0
      self.headers = {"User-Agent": "RedditNews/0.0.1"}
      if self.authorize():
        self.enabled = True
      else:
        print("there was a problem trying to authenticate the reddit api. check your reddit config.")
        self.enabled = False
        
  def authorize(self):
    '''
      authorizes the the reddit api based on config and adds auth to request headers.
      returns:
        bool: True upon sccess. Otherwise returns Flase
    '''
    auth = requests.auth.HTTPBasicAuth(self.id,self.secret)
    data = {
      "grant_type": "password",
      "username": self.username,
      "password": self.password
    }
    response = requests.post(self.authUrl, auth=auth, data=data, headers=self.headers).json()
    if response["access_token"]:
      self.authorizedAt = int(time.time())
      self.authExpires = int(response["expires_in"])
      token = response["access_token"]
      self.headers["Authorization"] = f"bearer {token}"
      return True
    return False

  def parseResponse(self, response):
    '''
      parses a response string from reddit into a list containing:
      headline, link and source.
      [
        {
          "headline": "Skirting US sanctions, Cubans flock to cryptocurrency to shop online, send funds",
          "link": "https://www.channelnewsasia.com/news/business/skirting-us-sanctions--cubans-flock-to-cryptocurrency-to-shop-online--send-funds-11901148",
          "source": "reddit"
        },
        ...
      ]
      params:
        response:str the text response from a reddit query.
      returns:
        list: list of parsed dicts from the response text.
    '''
    response = json.loads(response)["data"]["children"]
    result = []
    for post in response:
      result.append({
        "headline": post["data"]["title"],
        "link": post["data"]["url"],
        "source": "reddit",
      })
    return result
  
  def sendRequest(self,url,params={}):
    '''
      Sends a request to the reddit api with the authorized headers and the passed parameters.
      params:
        url:str the url to send the request to.
        params:dict a dictionary of reddit parameters.
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    #check if token didn't expire and reauthorize in such case or disable the api upon failure
    if time.time() >= self.authorizedAt+self.authExpires:
      if self.authorize():
        self.enabled = True
      else:
        print("there was a problem trying to authenticate the reddit api. check your reddit config.")
        self.enabled = False

    if self.enabled:
      #params.update({"apiKey": self.apiKey,"pageSize":1})
      response = requests.get(url,params=params,headers=self.headers).text
      response = self.parseResponse(response)
      return response if response else None
    else:
      return None
    
  def query(self,q):
    '''
      query the reddit api.
      params:
        q:str the query string to be passed to the api
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    return self.sendRequest(self.searchUrl,{"q": parse.quote_plus(q)})
  def trending(self):
    '''
      get the trending news from reddit.
      returns:
        str: result from the api containing the posts.
        None: if nothing is returned.
    '''
    return self.sendRequest(self.trendingUrl,None)
'''
#test
import settings
reddit=RedditAggregator(settings.redditConfig)
print(len(reddit.trending())) #should be 25
print(len(reddit.query("hi"))) #should be 25
'''