from main import news
import time
def test_main():
  '''
    this will test the main functionality as well as all the attached api handlers
  '''
  print("Detecting aggregators...")
  assert len(news.aggregators) > 0 #there is in fact at least one aggregator that is enabled
  print(str(len(news.aggregators))+" aggregators detected.")
  for i,aggregator in enumerate(news.aggregators): #check all enabled aggregators
    print("Checking aggregator #"+str(i+1)+"...")
    assert aggregator.enabled #check if the aggregator is correctly configured and instantiated using class inner checks
    print("Aggregator is authorized and enabled. testing queries...")
    assert len(aggregator.query("test")) > 0 #check if the query function returns results
    time.sleep(0.1) #sleeping for a 10th of a second to prevent http over requesting
    print("Queries are Okay, testing trends...")
    assert len(aggregator.trending()) > 0 #check if the trending function returns results
    print("Trends are okay.\nAggregator #"+str(i+1)+" is okay.")
  print("Everything is okay :)")