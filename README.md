
<h1>New Aggregator</h1>
<p>
This is a simple news app that loads news feeds from newsapi and reddit, Designed using Python's fastApi framework for building apis.
</p>
<h2>Requirements:</h2>
<p>Python 3.9.2+</p>
<h2>Installation:</h2>
<p>
To install this appilcation just clone this repository and execute the following: <br/>
<code>pip install -r requirements.txt</code><br/>
it is recommended to use a virtual environment while doing so if you are a developer or you're running other things using python. <br/>
</p>
<h2>Running:</h2>
<p>
to run the application navigate to the project directory using cmd or the terminal and execute the following.<br/>
<code>uvicorn main:app --reload</code><br/>
To run the application as a package navigate to the project directory using cmd or the terminal. Once inside of the project direcotry execute the following.<br/>
<code>python3 .</code><br/>
Or you can navigate to whereever the project root folder is accessible and execute the following<br/> command replacing projcet_root_folder with the actual project folder name.<br/>
<code>python3 project_root_folder</code>

</p>
<h2>Settings:</h2>
</p>
The app can be configured using a settings.py file to use your own api keys and credentials and other configurations should conventionally be put there as well.
</p>
<h2>Enabling and disabling Apis:</h2>
<p>
You can enable and disable any api that is currently in use by two means:

1- using the aggregators list:
this is a list of the currently active api instances was originally in settings but now can be located in main.py for convention and understandability of the high level file.
to disable an api just remove its handle instance from the aggregators list.
to add a new one just instantiate it as an element of the mentioned list.

2- programatically:
all the api handlers have aggregator.enable boolean which if set to false during programming or runtime, will render the api dormant.
</p>
<h2>Extending the app:</h2>
<p>
You can extend the functionality of the application by adding more apis to look up news from.

1- You should write an api handler that has the following methods:
<br/>

</p>

<code>
  def query(q:str) -> str | None:<br/>
    &nbsp;&nbsp;#takes a string query and returns a json string with search results or none in case<br/>
    &nbsp;&nbsp;none are found or errors occured.<br/>
    &nbsp;&nbsp;pass<br/>
  def trending() -> str | None:<br/>
    &nbsp;&nbsp;#returns a json string with trending results or none in case none are found or errors occured.<br/>
    &nbsp;&nbsp;pass<br/>
</code>
<p>
2- You should add some your config in settings.py in order to use them later in main.py on the instance.<br/>
3- Once done, add the new api handler instance to main.aggregators list and instantiate it with the config. <br/>
</p>
<h2>Testing:</h2>
<p>
The app is automatically tested using pytest, coverage on a gitlab CI/CD pipeline. check the CI/CD/pipelines in this repository to check that out.<br/><br/>
To manually test this project run: <br/>
<code>python3 -m pytest --cov-report=html --cov=. test.py</code><br/>
To programmatically test this project add this inside your code: <br/>
<code>
import test<br/>
test.test_main()<br/>
</code>
</p>
Happy Coding!
</p>
